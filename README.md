Bitcoin Price Trend Calculator
==============================

This program calculates whether you should buy, sell, or hold Bitcoin based on its price trend over the past one year.

Technologies Used
-----------------

- JavaScript
- Axios

Installation
------------

1. Clone the repository.
2. Install dependencies using `npm install`.

Usage
-----

Run the program using `npm start`.

The program will calculate the trend based on Bitcoin's price data from the past one year, obtained from the Coindesk
API. It will then output whether you should buy, sell, or hold Bitcoin based on the trend.

API Reference
-------------

This program uses the [Coindesk API](https://www.coindesk.com/coindesk-api) to obtain Bitcoin's historical price data.

License
-------

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
See [LICENSE](https://chat.openai.com/c/LICENSE) for more information.