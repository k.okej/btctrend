import axios, { AxiosResponse } from "axios";
import { HistoricalPriceData } from "./interface/HistoricalPriceData";
import { BitcoinPriceApiResponse } from "./interface/BitcoinPriceApiResponse";

const getHistoricalPriceData = async (
    startDate: string,
    endDate: string
): Promise<HistoricalPriceData[]> => {
    const response: AxiosResponse<BitcoinPriceApiResponse> = await axios.get(
        `https://api.coindesk.com/v1/bpi/historical/close.json?start=${ startDate }&end=${ endDate }&currency=pln`
    );

    return Object.entries(response.data.bpi).map(([ time, close ]: [ string, number ]) => ({
        time: new Date(time),
        close,
    }));
};

const calculateTrend = async (): Promise<void> => {
    const currentDate = new Date();
    const endDate = currentDate.toISOString().slice(0, 10);
    const startDate = new Date(currentDate.setFullYear(currentDate.getFullYear() - 1)) // one year ago
        .toISOString()
        .slice(0, 10);

    const data = await getHistoricalPriceData(startDate, endDate);
    const currentPrice = data[data.length - 1].close;
    const averagePrice = data.reduce((sum, { close }) => sum + close, 0) / data.length;
    const variance = data.reduce((sum, { close }) => sum + (close - averagePrice) ** 2, 0) / data.length;
    const standardDeviation = Math.sqrt(variance);

    if (currentPrice < averagePrice - standardDeviation) {
        console.log('Buy Bitcoin');
    } else if (currentPrice > averagePrice + standardDeviation) {
        console.log('Sell Bitcoin');
    } else {
        console.log('Hold Bitcoin');
    }
};

calculateTrend();