export interface HistoricalPriceData {
    time: Date;
    close: number;
}