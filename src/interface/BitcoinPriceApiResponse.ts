export interface BitcoinPriceApiResponse {
    bpi: {
        [date: string]: number;
    };
}